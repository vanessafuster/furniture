package furniture.data;

public abstract class Furniture {

	private String model = null;
	private String manufacturer = null;
	private String mainMaterial = null;
	private String mainColor = null;
	private double width = 0; // valor mayor que cero.
	private double high = 0; // valor mayor que cero.
	private double depth = 0; // valor mayor que cero.
	
	public Furniture(String model, String manufacturer, String mainMaterial, 
			String mainColor, double width,	double high, double depth) {
		this.model = model;
		this.manufacturer = manufacturer;
		this.mainMaterial = mainMaterial;
		this.mainColor = mainColor;
		this.width = width;
		this.high = high;
		this.depth = depth;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getMainMaterial() {
		return mainMaterial;
	}

	public void setMainMaterial(String mainMaterial) {
		this.mainMaterial = mainMaterial;
	}

	public String getMainColor() {
		return mainColor;
	}

	public void setMainColor(String mainColor) {
		this.mainColor = mainColor;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		if(width<=0) {
			System.out.println("El valor del largo debe ser mayor que cero");
		}
		else {
			this.width = width;
		}
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		if (high<=0) {
			System.out.println("El valor de la altura debe ser mayor que cero");
		}
		else {
			this.high = high;
		}
	}

	public double getDepth() {
		return depth;
	}

	public void setDepth(double depth) {
		if(depth<=0) {
			System.out.println("El valor del ancho debe ser mayor que cero");
		}
		else {
			this.depth = depth;
		}
	}

	@Override
	public String toString() {
		return  model + ";" + manufacturer +";" + mainMaterial + ";" + mainColor + 
				";" + width + ";" + high + ";" + depth + ";";
	}
	
	public String MostrarConsola() {
		return "Wardrobe [ modelo: " + getModel()+", fabricante: " + 
				getManufacturer()+", material: "+ getMainMaterial()+", color: "
				+getMainColor() +", ancho: " + getWidth() + ", alto: " + 
				getHigh() + ", largo: " + getDepth() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((manufacturer == null) ? 0 : manufacturer.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Furniture other = (Furniture) obj;
		if (manufacturer == null) {
			if (other.manufacturer != null)
				return false;
		} else if (!manufacturer.equals(other.manufacturer))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		return true;
	}

	public int compareTo(Furniture object) {
		return this.getModel().compareTo(object.getModel());
	}
	
	public double volumen() {
		double volumen=this.getDepth()*this.getHigh()*this.getWidth();
		return volumen;
	}
	

}
