package furniture.data;

public class Wardrobe extends Furniture {
	private int foots;
	private int doors;
	private int drawers;
	public Wardrobe(String model, String manufacturer, String mainMaterial, 
			String mainColor, double width, double high, double depth,
			int foots, int doors, int drawers) {
		super(model, manufacturer, mainMaterial, mainColor, width, high, depth);
		this.foots = foots;
		this.doors = doors;
		this.drawers = drawers;
	}
	public int getFoots() {
		return foots;
	}
	public void setFoots(int foots) {
		if(foots<=0) {
			System.out.println("el número de patas no puede ser igual o menor"
					+ " que cero");
		}
		else {
			this.foots = foots;
		}
	}
	public int getDoors() {
		return doors;
	}
	public void setDoors(int doors) {
		if(doors<=0) {
			System.out.println("el número de puertas no puede ser igual o menor"
					+ " que cero");
		}
		else {
			this.doors = doors;
		}
	}
	public int getDrawers() {
		return drawers;
	}
	public void setDrawers(int drawers) {
		if(drawers<=0) {
			System.out.println("el número de cajones no puede ser igual o menor"
					+ " que cero");
		}
		else {
			this.drawers = drawers;
		}
	}
	
	@Override
	public String toString() {
		return "wardrobe;" + getModel()+";" + getManufacturer()+";"+ 
				getMainMaterial()+";"+getMainColor() +";" + getWidth() + ";" + 
				getHigh() + ";" + getDepth() +";"+ foots + ";" + doors +";"
				+ drawers +";";
	}
	
	@Override
	public String MostrarConsola() {
		return "Wardrobe [ modelo: " + getModel()+", fabricante: " + 
				getManufacturer()+", material: "+ getMainMaterial()+", color: "
				+getMainColor() +", ancho: " + getWidth() + ", alto: " + 
				getHigh() + ", largo: " + getDepth() +", nº patas: "+ foots +
				", nº puertas: " + doors+ ", nº cajones: " + drawers+ "]";
	}
}
