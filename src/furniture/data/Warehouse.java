package furniture.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class Warehouse {
	private  String name = null;
	private  Map<String, Furniture> products = new HashMap<String, Furniture>();
	
	public Warehouse(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Map<String, Furniture> getProducts() {
		return products;
	}
//LEE EL ARCHIVO QUE SE LE PASA POR PARÁMETRO Y LO VUELCA EN EL MAP	
	public void LoadFile(String file) {
	// Se comprueba si el fichero existe
		if(!(new File(file)).exists())	{
			System.out.println("No he encontrado el fichero " + file);
		}
		else {
			try	{//se lee el fichero
				BufferedReader ficheroEntrada = new BufferedReader(
						new FileReader(new File(file)));
				String linea = ficheroEntrada.readLine();
				if(linea == null) {
					System.out.println("El fichero está vacío.");
				}
				else {
					while(linea != null) {
						//se parte cada línea por los separadores (;)
						String[] datosSeparados = linea.split(";");						
						if(datosSeparados[0].equalsIgnoreCase("chair")) {
							AddProduct(new Chair(datosSeparados[1],
									datosSeparados[2], 
									datosSeparados[3],
									datosSeparados[4],
									Double.parseDouble((datosSeparados[5])
											.replaceAll(",", ".")),
									Double.parseDouble((datosSeparados[6])
											.replaceAll(",", ".")),
									Double.parseDouble((datosSeparados[7])
											.replaceAll(",", ".")),
									Integer.parseInt(datosSeparados[8]),
									Boolean.parseBoolean(datosSeparados[9])));					
						}
						else if(datosSeparados[0].equalsIgnoreCase("table")) {
							AddProduct(new Table(datosSeparados[1],
									datosSeparados[2], 
									datosSeparados[3],
									datosSeparados[4],
									Double.parseDouble((datosSeparados[5])
											.replaceAll(",", ".")),
									Double.parseDouble((datosSeparados[6])
											.replaceAll(",", ".")),
									Double.parseDouble((datosSeparados[7])
											.replaceAll(",", ".")),
									Integer.parseInt(datosSeparados[8])));							
						}
						else if (datosSeparados[0].equalsIgnoreCase("wardrobe")) {
							AddProduct(new Wardrobe(datosSeparados[1],
									datosSeparados[2], 
									datosSeparados[3],
									datosSeparados[4],
									Double.parseDouble((datosSeparados[5])
											.replaceAll(",", ".")),
									Double.parseDouble((datosSeparados[6])
											.replaceAll(",", ".")),
									Double.parseDouble((datosSeparados[7])
											.replaceAll(",", ".")),
									Integer.parseInt(datosSeparados[8]),
									Integer.parseInt(datosSeparados[9]),
									Integer.parseInt(datosSeparados[10])));						
						}
					linea=ficheroEntrada.readLine();	
					}
				}
				ficheroEntrada.close();//cierre lectura del fichero
			}catch(IOException errorDeFichero)	{
					System.out.println("Ha habido problemas: "+ 
							errorDeFichero.getMessage());
				}
		}	
	}
//GUARDA EL MAPA EN EL FICHERO SOBREESCRIBIENDO EL MISMO	
	public void SaveFile(String file) {
		PrintWriter printWriter = null;
		try {
			printWriter = new PrintWriter (file);
			for (Map.Entry <String, Furniture> entry :  getProducts().entrySet()) {
				printWriter.println(entry.getValue().toString());
			}	
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if ( printWriter != null ) {
				printWriter.close();
			}
		}		
	}
//MUESTRA, SI ESTÁ EN EL ALMACÉN, EL PRODUCTO CUYO MODELO HA PASADO POR PARÁMETRO	
	public void ShowProduct(String model) {
		if(getName().equalsIgnoreCase("warehouseA")) {
			if(getProducts().containsKey(model)) {
				System.out.println("El producto buscado se encuentra en el "
						+ "Almacen A");	
				System.out.println();
				System.out.println(getProducts().get(model).MostrarConsola());
			}
			else {
				System.out.println("El producto buscado no se encuentra en "
						+ "Almacen A.");
			}
		}
		else if(getName().equalsIgnoreCase("warehouseB")) {
			if(getProducts().containsKey(model)){
				System.out.println("El producto buscado se encuentra en el "
						+ "Almacen B");
				System.out.println();
				System.out.println(getProducts().get(model).MostrarConsola());
			}
			else {
				System.out.println("El producto buscado no se encuentra en "
						+ "Almacen B.");
			}
		}		
	}
//AÑADE EL OBJETO PASADO COMO PARÁMETRO AL MAPA
//DEVUELVE TRUE SI EL PROCESO HA SIDO CORRECTO
	public boolean AddProduct(Furniture obj) {	
		boolean existeProducto=false;
		if(!getProducts().containsKey(obj.getModel())) {
			products.put(obj.getModel(),obj);
			if(getProducts().containsKey(obj.getModel())) {
				existeProducto=true;
			}
		}
		return existeProducto;
	}
//ELIMINA DEL MAPA EL OBJETO CUYO MODELO ES PASADO POR PARÁMETRO
//DEVUELVE TRUE SI EL PROCESO HA SIDO CORRECTO
	public boolean DelProduct(String model) {
		boolean productoExiste=false;
		if(getProducts().containsKey(model)) {
			products.remove(model);
			System.out.println("Producto eliminado correctamente.");
			if(!getProducts().containsKey(model)) {
				productoExiste=true;
			}
		}
		return productoExiste;
	}
	
	public void ListProducts() {
		for (Map.Entry <String, Furniture> entry :  getProducts().entrySet()) {
			System.out.println(entry.getValue().MostrarConsola());
		}
	}
}
