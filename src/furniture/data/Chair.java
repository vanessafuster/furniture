package furniture.data;

public class Chair extends Furniture {
	private int foots;
	private boolean backrest;

	public Chair(String model, String manufacturer, String mainMaterial, 
			String mainColor, double width, double high, double depth, 
			int foots, boolean backrest) {
		super(model, manufacturer, mainMaterial, mainColor, width, high, depth);
		this.foots = foots;
		this.backrest = backrest;
	}

	public int getFoots() {
		return foots;
	}

	public void setFoots(int foots) {
		if(foots<=0) {
			System.out.println("el número de patas no puede ser igual o menor"
					+ " que cero");
		}
		else {
			this.foots = foots;
		}
	}

	public boolean isBackrest() {
		return backrest;
	}

	public void setBackrest(boolean backrest) {
		this.backrest = backrest;
	}

	@Override
	public String toString() {
		return "chair;" + getModel()+";" + getManufacturer()+";"+ 
				getMainMaterial()+";"+getMainColor() +";"+ getWidth() + ";" + 
				getHigh() + ";" + getDepth() +";"+ foots + ";" + backrest +";";
	}
	
	@Override
	public String MostrarConsola() {
		String respaldo;
		if (backrest==true){
			respaldo="sí";
		}
		else respaldo="no";
		
		return "Chair[ modelo: " + getModel()+", fabricante: " + 
				getManufacturer()+", material: "+ getMainMaterial()+", color: "
				+getMainColor() +", ancho: " + getWidth() + ", alto: " + 
				getHigh() + ", largo: " + getDepth() +", nº patas: "+ foots +
				", tiene respaldo: " + respaldo+"]";
	}
	
}
