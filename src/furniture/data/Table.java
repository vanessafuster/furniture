package furniture.data;

public class Table extends Furniture {
	
	private int foots;

	public Table(String model, String manufacturer, String mainMaterial, 
			String mainColor, double width, double high, double depth, 
			int foots) {
		super(model, manufacturer, mainMaterial, mainColor, width, high, depth);
		this.foots = foots;
	}

	public int getFoots() {
		return foots;
	}

	public void setFoots(int foots) {
		if(foots<=0) {
			System.out.println("el número de patas no puede ser igual o menor"
					+ " que cero");
		}
		else {
			this.foots = foots;
		}
	}

	@Override
	public String toString() {
		return "Table;" + getModel()+";" + getManufacturer()+";"+ 
				getMainMaterial()+";"+getMainColor() +";" +  getWidth() + ";" +
				getHigh() + ";" + getDepth() +";"+ foots + ";" ;
	}
	
	@Override
	public String MostrarConsola() {
		return "Table [ modelo: " + getModel()+", fabricante: " + 
				getManufacturer()+", material: "+ getMainMaterial()+", color: "
				+getMainColor() +", ancho: " + getWidth() + ", alto: " + 
				getHigh() + ", largo: " + getDepth() +", nº patas: "+ foots +
				"]";
	}
	
}
