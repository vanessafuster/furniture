package furniture.main;

import java.util.Scanner;

import furniture.data.Chair;
import furniture.data.Table;
import furniture.data.Wardrobe;
import furniture.data.Warehouse;


public class FurnitureApp {
	
	public static void main(String[] args) {
		
//DECLARACIÓN DE VARIABLES
		byte opcionMenu, opcionAlmacen;
		Warehouse almacen = null;
		String modeloProducto;
		
//CREACIÓN DE ALMACENES		
		Warehouse warehouseA =new Warehouse("warehouseA");
		Warehouse warehouseB = new Warehouse("warehouseB");

//VOLCADO DE LOS FICHEROS EN LOS ALMACENES
		warehouseA.LoadFile("warehouseA.csv");
		warehouseB.LoadFile("warehouseB.csv");
		
		
		Scanner scanner = new Scanner (System.in);
		
//MENÚ PRINCIPAL
		do {	
			do {
				System.out.println();
				System.out.println("---MENÚ PRINCIPAL FURNITURE---");
				System.out.println("1. Listado almacén A.");
				System.out.println("2. Listado almacén B.");
				System.out.println("3. Buscar producto.");
				System.out.println("4. Añadir producto.");
				System.out.println("5. Eliminar producto.");
				System.out.println("------------------------------");
				System.out.println("0. Salir del programa.");
				System.out.println();
				System.out.println("Elije una opción del menú:");
				while (!scanner.hasNextByte()) {
					System.out.println("Introduzca un número entero del "
							+ "1 al 5");
					System.out.println("0 para salir.");
					scanner.nextLine();
				}
				opcionMenu=scanner.nextByte();
				scanner.nextLine();
			}while(!(opcionMenu>=0)&&(opcionMenu<=5));
			
			
			System.out.println();
		
			switch (opcionMenu) {
				case 1: //listado almacén A
					System.out.println("---LISTADO ALMACEN A---");
					warehouseA.ListProducts();
					break;
				case 2: //listado almacén B
					System.out.println("---LISTADO ALMACEN B---");
					warehouseB.ListProducts();
					break;
				case 3: //buscar producto
					System.out.println("---BUSCAR PRODUCTO---");
					System.out.println("Introduzca el modelo del producto a "
							+ "buscar");
					while(!scanner.hasNext()) {
						System.out.println("Introduzca el modelo");
						scanner.nextLine();
					}
					modeloProducto = scanner.nextLine();
					warehouseA.ShowProduct(modeloProducto);
					warehouseB.ShowProduct(modeloProducto);
					break;
				case 4: //añadir producto
					do {
							System.out.println("---AÑADIR PRODUCTO---");
							System.out.println("Indique en qué almacén quiere "
									+ "añadir el producto:");
							System.out.println("1. Almacen A.");
							System.out.println("2. Almacen B.");
							while (!scanner.hasNextByte()) { 
								System.out.println("Introduzca un número entero"
										+ " del 1 al 2");
								scanner.nextLine();
							}
							opcionAlmacen=scanner.nextByte();
							scanner.nextLine();
					}while(!(opcionAlmacen==1 || opcionAlmacen==2));
					if (opcionAlmacen==1)
						almacen = warehouseA;
					else if(opcionAlmacen==2)
						almacen= warehouseB;
					AnadirProducto(almacen);
					break;
				case 5: //eliminar producto
					System.out.println("---ELIMINAR PRODUCTO---");
					System.out.println("Indique en qué almacén quiere eliminar"
							+ " el producto:");
					System.out.println("1. Almacén A.");
					System.out.println("2. Almacén B.");
					while (!scanner.hasNextByte()) 
						System.out.println("Introduzca un número entero del"
								+ " 1 al 2");
						opcionAlmacen=scanner.nextByte();
						scanner.nextLine();
						if (opcionAlmacen==1)
							almacen = warehouseA;
						else if(opcionAlmacen==2)
							almacen= warehouseB;
					EliminarProducto(almacen);
					break;
				case 0: //salir del programa
					System.out.println("---SALIR---");
					warehouseA.SaveFile("warehouseA.csv");
					warehouseB.SaveFile("warehouseB.csv");
					System.out.println("guardando ficheros...");
					System.out.println("Fin del programa");
					System.exit(0);
	//guardar el contendido de los almacenes en sus ficheros correspondientes
					
					break;
				default: System.out.println("Opción no válida");
					break;
			}
		}while (opcionMenu!=0);		
		scanner.close();
	}//FIN DEL MAIN
	
	//public static void BuscarProducto(String modelo) {
//debe buscar en los dos almacenes	
	
	//}
	private static void AnadirProducto(Warehouse almacen) {
		//DECLARACIÓN DE VARIABLES DEL MÉTODO
		String tipo;
		String modelo;
		String fabricante;
		String material;
		String color;
		Double ancho;
		Double alto;
		Double largo;
		int numeroPatas;
		String tieneRespaldo;
		boolean respaldo = false;
		int numeroPuertas;
		int numeroCajones;
		
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.println("Introduzca el tipo de mueble"
					+ " (silla/mesa/guardarropa):");
			tipo=sc.nextLine();
		}while(!tipo.equalsIgnoreCase("silla") && 
				!tipo.equalsIgnoreCase("mesa")&& 
				!tipo.equalsIgnoreCase("guardarropa"));	
	//Si se quiere introducir una SILLA
		if(tipo.equalsIgnoreCase("silla")) {
			System.out.println("Introduzca Modelo:");
			while(!sc.hasNextLine()) {
				System.out.println("El campo modelo no puede estar vacío");
				sc.nextLine();
			}
			modelo=sc.nextLine();
			System.out.println("Introduzca Fabricante:");
			while(!sc.hasNextLine()) {
				System.out.println("El campo fabricante no puede estar vacío");
				sc.nextLine();
			}
			fabricante=sc.nextLine();
			System.out.println("Introduzca Material Principal:");
			while(!sc.hasNextLine()) {
				System.out.println("El campo material no puede estar vacío");
				sc.nextLine();
			}
			material=sc.nextLine();
			System.out.println("Introduzca Color:");
			while(!sc.hasNextLine()) {
				System.out.println("El campo color no puede estar vacío");
				sc.nextLine();
			}
			color=sc.nextLine();
			System.out.println("Introduzca el ancho (0,00)");
			while(!sc.hasNextDouble()) { 
				System.out.println("Introduzca el ancho, utilice la (,) "
						+ "para los decimales");
				sc.nextLine();
			}
			ancho=sc.nextDouble();
			sc.nextLine();
			System.out.println("Introduzca el alto (0,00)");
			while(!sc.hasNextDouble()) {
				System.out.println("Introduzca el alto, utilice la (,) "
						+ "para los decimales");
				sc.nextLine();
			}
			alto=sc.nextDouble();
			sc.nextLine();
			System.out.println("Introduzca el largo (0,00)");
			while(!sc.hasNextDouble()) { 
				System.out.println("Introduzca el largo, utilice la (,) "
						+ "para los decimales");
				sc.nextLine();
			}
			largo=sc.nextDouble();
			sc.nextLine();
			System.out.println("Introduzca el número de patas:");
			while(!sc.hasNextInt()) {
				System.out.println("El campo número de patas no puede estar"
						+ " vacío");
				sc.nextLine();
			}	
			numeroPatas=sc.nextInt();
			sc.nextLine();
			do{
				System.out.println("Tiene respaldo? (si/no)");
				tieneRespaldo = sc.nextLine();
			}while(!tieneRespaldo.equalsIgnoreCase("si")&&
					(!tieneRespaldo.equalsIgnoreCase("no")));
			if(tieneRespaldo.equalsIgnoreCase("si")) {
				respaldo=true;
			}
			else if(tieneRespaldo.equalsIgnoreCase("no")) {
				respaldo=false;
			}
//SE LLAMA AL MÉTODO ADDPRODUCT DESDE EL ALMACÉN CORRESPONDIENTE PASANDO 
//POR PARÁMETRO EL PRODUCTO
			almacen.AddProduct(new Chair( modelo, fabricante, 
						material, color, ancho, alto, largo, numeroPatas,
						respaldo));
		}	
	//Si se quiere introducir una MESA
		else if(tipo.equalsIgnoreCase("mesa")) {
			System.out.println("Introduzca Modelo:");
			while(!sc.hasNextLine())
				System.out.println("El campo modelo no puede estar vacío");
			modelo=sc.nextLine();
			System.out.println("Introduzca Fabricante:");
			while(!sc.hasNextLine())
				System.out.println("El campo fabricante no puede estar vacío");
			fabricante=sc.nextLine();
			System.out.println("Introduzca Material Principal:");
			while(!sc.hasNextLine())
				System.out.println("El campo material no puede estar vacío");
			material=sc.nextLine();
			System.out.println("Introduzca Color:");
			while(!sc.hasNextLine())
				System.out.println("El campo color no puede estar vacío");
			color=sc.nextLine();
			System.out.println("Introduzca el ancho (0,00)");
			while(!sc.hasNextDouble()) { 
				System.out.println("Introduzca el ancho, utilice la (,) "
						+ "para los decimales");
				sc.nextLine();
			}
			ancho=sc.nextDouble();
			sc.nextLine();
			System.out.println("Introduzca el alto (0,00)");
			while(!sc.hasNextDouble()) { 
				System.out.println("Introduzca el alto, utilice la (,) "
						+ "para los decimales");
				sc.nextLine();
			}
			alto=sc.nextDouble();
			sc.nextLine();
			System.out.println("Introduzca el largo (0,00)");
			while(!sc.hasNextDouble()) {
				System.out.println("Introduzca el largo, utilice la (,) "
						+ "para los decimales");
				sc.nextLine();
			}
			largo=sc.nextDouble();
			sc.nextLine();
			System.out.println("Introduzca el número de patas:");
			while(!sc.hasNextInt()) {
				System.out.println("El campo número de patas no puede estar"
						+ " vacío");
				sc.nextLine();
			}
			numeroPatas=sc.nextInt();
			sc.nextLine();
			
//SE LLAMA AL MÉTODO ADDPRODUCT DESDE EL ALMACÉN CORRESPONDIENTE PASANDO 
//POR PARÁMETRO EL PRODUCTO				
			almacen.AddProduct(new Table( modelo, fabricante, 
						material, color, ancho, alto, largo, numeroPatas));
		}
	
	//Si se quiere introducir un GUARDARROPA
		else if(tipo.equalsIgnoreCase("guardarropa")) {
			System.out.println("Introduzca Modelo:");
			while(!sc.hasNextLine())
				System.out.println("El campo modelo no puede estar vacío");
			modelo=sc.nextLine();
			System.out.println("Introduzca Fabricante:");
			while(!sc.hasNextLine())
				System.out.println("El campo fabricante no puede estar vacío");
			fabricante=sc.nextLine();
			System.out.println("Introduzca Material Principal:");
			while(!sc.hasNextLine())
				System.out.println("El campo material no puede estar vacío");
			material=sc.nextLine();
			System.out.println("Introduzca Color:");
			while(!sc.hasNextLine())
				System.out.println("El campo color no puede estar vacío");
			color=sc.nextLine();
			System.out.println("Introduzca el ancho (0,00)");
			while(!sc.hasNextDouble()) {
				System.out.println("Introduzca el ancho, utilice la (,) "
						+ "para los decimales");
				sc.nextLine();
			}
			ancho=sc.nextDouble();
			sc.nextLine();
			System.out.println("Introduzca el alto (0,00)");
			while(!sc.hasNextDouble()) {
				System.out.println("Introduzca el alto, utilice la (,) "
						+ "para los decimales");
				sc.nextLine();
			}
			alto=sc.nextDouble();
			sc.nextLine();
			System.out.println("Introduzca el largo (0,00)");
			while(!sc.hasNextDouble()) {
				System.out.println("Introduzca el largo, utilice la (,) "
						+ "para los decimales");
				sc.nextLine();
			}
			largo=sc.nextDouble();
			sc.nextLine();
			System.out.println("Introduzca el número de patas:");
			while(!sc.hasNextInt()) {
				System.out.println("El campo número de patas no puede estar"
						+ " vacío");
				sc.nextLine();
			}
			numeroPatas=sc.nextInt();
			sc.nextLine();
			System.out.println("Introduzca el número de puertas:");
			while(!sc.hasNextInt()) {
				System.out.println("El campo número de puertas no puede "
						+ "estar vacío");
				sc.nextLine();
			}
			numeroPuertas=sc.nextInt();
			sc.hasNextLine();
			System.out.println("Introduzca el número de cajones");
			while(!sc.hasNextInt()) {
				System.out.println("El campo número de cajones no puede "
						+ "estar vacío");
				sc.nextLine();
			}
			numeroCajones = sc.nextInt();
			sc.nextLine();
//SE LLAMA AL MÉTODO AddProduct DESDE EL ALMACÉN CORRESPONDIENTE PASANDO POR
//PARÁMETRO EL PRODUCTO				
			almacen.AddProduct(new Wardrobe( modelo, fabricante, 
					material, color, ancho, alto, largo, numeroPatas,
					numeroPuertas, numeroCajones));
		}
		
		//sc.close();	Se deja abierto el scanner y se añade un resource porque 
		//de otro modo se cierra también en el main y no puede leer el menú
	}
			
	private static void EliminarProducto(Warehouse almacen) {
		//VARIABLES PROPIAS DEL MÉTODO
		String modelo;
		
		@SuppressWarnings("resource")
		Scanner scanner =new Scanner(System.in);
		
		do {
			System.out.println("Introduzca el modelo del producto que quiere "
				+ "eliminar:");
			modelo=scanner.nextLine();
		}while(modelo.isEmpty());
//SE LLAMA AL MÉTODO DelProduct DESDE EL ALMACÉN CORRESPONDIENTE PASANDO POR
//PARÁMETRO EL MODELO
		almacen.DelProduct(modelo);
		//scanner.close();
	}
	
}
